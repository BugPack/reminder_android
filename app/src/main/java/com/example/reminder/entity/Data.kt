package com.example.reminder.entity

import java.io.Serializable

data class Data(
    var title: String,
    var description: String,
    var time: Time
) : Serializable