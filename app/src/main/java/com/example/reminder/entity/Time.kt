package com.example.reminder.entity

import java.io.Serializable

data class Time(
    var end: Long
) : Serializable