package com.example.reminder.ui.adapter

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.reminder.R
import com.example.reminder.entity.Data
import kotlinx.android.synthetic.main.item_list.view.*
import java.util.*

class RecyclerAdapter(private val context: Context) :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    val dataset = arrayListOf<Data>()
    private val handler = Handler()
    private val listHolders = arrayListOf<ViewHolder>()
    private val updateRemainingTimeRunnable = Runnable {
        synchronized(listHolders) {
            val currentTime = System.currentTimeMillis()
            for (holder: ViewHolder in listHolders) {
                holder.updateTimeRemaining(currentTime)
            }
        }
    }

    init {
        startUpdateTimer()
    }

    private fun startUpdateTimer() {
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(updateRemainingTimeRunnable)
            }
        }, 1000, 1000)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataset[position]
        holder.setData(item)

        holder.delete.setOnClickListener {
            removeItem(item)
            Toast.makeText(context, "Удалено", Toast.LENGTH_SHORT).show()
        }

        synchronized(listHolders) {
            listHolders.add(holder)
        }
    }

    fun addItem(data: Data) {
        if (!dataset.contains(data)) {
            dataset.add(data)
            notifyDataSetChanged()
            Toast.makeText(context, "Добавлено", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Такой уже есть", Toast.LENGTH_SHORT).show()
        }
    }

    fun removeItems() {
        dataset.clear()
        notifyDataSetChanged()
    }

    fun removeItem(data: Data) {
        dataset.remove(data)
        notifyDataSetChanged()
    }

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var title = item.title
        var description = item.description
        var timeEnd = item.timeEnd
        var delete = item.delete
        lateinit var mData: Data

        fun setData(data: Data) {
            mData = data
            title.text = data.title
            description.text = data.description
            timeEnd.text = data.time.end.toString()
        }

        fun updateTimeRemaining(currentTime: Long) {
            val timeDiff = mData.time.end - currentTime
            if (timeDiff > 0) {
                val sec = (timeDiff / 1000) % 60
                val min = (timeDiff / (1000 * 60)) % 60
                val hour = (timeDiff / (1000 * 60 * 60)) % 24
                val day = (timeDiff / (1000 * 60 * 60 * 24))

                val time = String.format("%02d:%02d:%02d:%02d", day, hour, min, sec)
                timeEnd.text = time
            } else {
                timeEnd.text = "Done"
            }
        }
    }
}