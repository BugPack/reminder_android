package com.example.reminder.ui.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.example.reminder.R
import com.example.reminder.entity.Data
import com.example.reminder.ui.adapter.RecyclerAdapter
import com.example.reminder.ui.fragment.CustomDialog
import com.example.reminder.utils.CacheUtils
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity() {
    private var adapter: RecyclerAdapter? = null
    private val DIALOG = "dialog"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initListItems()
    }

    override fun onPause() {
        super.onPause()
        adapter?.dataset?.let {
            CacheUtils
                .setData(it)
                .observeOn(Schedulers.io())
                .subscribe({}, { e ->
                    e.printStackTrace()
                })
        }
    }

    override fun onResume() {
        super.onResume()
        adapter?.dataset?.let {
            CacheUtils
                .getData()
                .observeOn(Schedulers.io())
                .subscribe({
                    adapter?.dataset?.clear()
                    adapter?.dataset?.addAll(it)
                }, { e ->
                    e.printStackTrace()
                })
        }
    }

    private fun initListItems() {
        if (adapter == null) {
            adapter = RecyclerAdapter(this)
        }
        listView?.adapter = adapter
        listView?.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId

        when (id) {
            R.id.add -> {
                addItem()
            }
//            R.id.edit -> {
//                Toast.makeText(this, "edit", Toast.LENGTH_SHORT).show()
//            }
            R.id.delete -> {
                removeItems()
            }
        }
        return true
    }

    private fun addItem() {
        val dialog = CustomDialog.getInstance()
        dialog.setOkClickListener(object : CustomDialog.OnOkClickListener {
            override fun onOkClicked(data: Data) {
                data.time.end = data.time.end + System.currentTimeMillis()
                adapter?.addItem(data)
            }
        }).show(supportFragmentManager, DIALOG)
    }

    private fun removeItems() {
        adapter?.removeItems()
    }
}
