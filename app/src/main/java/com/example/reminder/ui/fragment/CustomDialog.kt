package com.example.reminder.ui.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.reminder.R
import com.example.reminder.entity.Data
import com.example.reminder.entity.Time
import com.example.reminder.utils.TimerUtils

class CustomDialog : DialogFragment() {
    companion object {
        fun getInstance() = CustomDialog()
    }

    interface OnOkClickListener {
        fun onOkClicked(data: Data)
    }

    var dialog: AlertDialog? = null
    var title: EditText? = null
    var description: EditText? = null
    var timeDays: EditText? = null
    var timeHours: EditText? = null
    var timeMin: EditText? = null
    var timeSec: EditText? = null
    var btnOk: Button? = null
    var listener: OnOkClickListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_add, null)

        dialog = AlertDialog.Builder(context)
            .setView(view)
            .create()

        title = view?.findViewById(R.id.etTitle)
        description = view?.findViewById(R.id.etDescription)
        timeDays = view?.findViewById(R.id.etTimeDays)
        timeHours = view?.findViewById(R.id.etTimeHours)
        timeMin = view?.findViewById(R.id.etTimeMinutes)
        timeSec = view?.findViewById(R.id.etTimeSeconds)
        btnOk = view?.findViewById(R.id.btnOk)

        btnOk?.setOnClickListener {
            listener?.onOkClicked(getFieldsInfo())
            dismiss()
        }

        return dialog!!
    }

    fun setOkClickListener(listener: OnOkClickListener): CustomDialog {
        this.listener = listener
        return this
    }

    private fun getFieldsInfo(): Data {
        val title = title?.text.toString()
        val description = description?.text.toString()

        var mTimeDays = 0L
        if (!timeDays?.text.isNullOrEmpty()) {
            mTimeDays = timeDays?.text.toString().toLong()
        }
        var mTimeHours = 0L
        if (!timeHours?.text.isNullOrEmpty()) {
            mTimeHours = timeHours?.text.toString().toLong()
        }
        var mTimeMin = 0L
        if (!timeMin?.text.isNullOrEmpty()) {
            mTimeMin = timeMin?.text.toString().toLong()
        }
        var mTimeSec = 0L
        if (!timeSec?.text.isNullOrEmpty()) {
            mTimeSec = timeSec?.text.toString().toLong()
        }
        val timeEnd = TimerUtils.convertToMillis(mTimeDays, mTimeHours, mTimeMin, mTimeSec)
        return Data(title, description, Time(timeEnd))
    }
}