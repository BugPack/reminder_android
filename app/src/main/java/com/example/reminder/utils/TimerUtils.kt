package com.example.reminder.utils

object TimerUtils {
    fun convertToMillis(day: Long, hour: Long, min: Long, sec: Long): Long {
        val mSec = sec * 1000
        val mMin = min * 1000 * 60
        val mHour = hour * 1000 * 60 * 60
        val mDay = day * 1000 * 60 * 60 * 24
        return mDay + mHour + mMin + mSec
    }
}