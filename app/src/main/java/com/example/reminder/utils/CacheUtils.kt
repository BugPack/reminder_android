package com.example.reminder.utils

import android.content.Context
import com.example.reminder.App
import com.example.reminder.entity.Data
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.util.*

object CacheUtils {
    private const val DATA_DB = "data.db"

    fun setData(dataList: ArrayList<Data>): Observable<Completable> {
        return saveFile(DATA_DB, dataList)
    }

    fun getData(): Observable<ArrayList<Data>> {
        return loadFile(DATA_DB)
    }

    private fun saveFile(dbname: String, obj: Serializable): Observable<Completable> {
        return Observable.create<Completable> { emitter ->
            val fos = App.INSTANCE.openFileOutput(dbname, Context.MODE_PRIVATE)
            val oos = ObjectOutputStream(fos)
            oos.writeObject(obj)
            oos.close()
            fos.close()
            emitter.onNext(Completable.complete())
        }.subscribeOn(Schedulers.io())
    }

    private fun <T> loadFile(dbname: String): Observable<T> {
        return Observable.create<T> { emitter ->
            val fis = App.INSTANCE.openFileInput(dbname)
            val ois = ObjectInputStream(fis)
            val obj = ois.readObject() as T
            ois.close()
            fis.close()
            emitter.onNext(obj)
        }.subscribeOn(Schedulers.io())
    }

    fun clearCache() {
        App.INSTANCE.deleteFile(DATA_DB)
    }
}